#![feature(try_from)]

extern crate shells;
extern crate canonical_json;
extern crate serde_json;
extern crate base58check;
extern crate ed25519_dalek;
extern crate sha2;
extern crate base64;
extern crate serde;
extern crate clap;
#[macro_use]
extern crate serde_derive;

use std::time::SystemTime;
use clap::{App,Arg};
use canonical_json::Value;
use std::convert::TryFrom;
use base58check::*;
use sha2::Sha512;
use ed25519_dalek::SecretKey;
use ed25519_dalek::Keypair;
use ed25519_dalek::Signature;
use std::str;
use std::fs;
use std::fs::*;
#[derive(Serialize, Deserialize, Debug)]
struct SignedRequest {
    dst_address: String,
    snd_address: String,
    tx_value: u64,
    timestamp: u64,
    signature: String,
}
#[derive(Serialize, Deserialize, Debug)]
struct SignedRequestForced {
    dst_address:String,
    signature:String,
}
fn sign_json_forced(secret_key:String,public_key:String,json:String) -> (){
    let contents = fs::read_to_string(json).unwrap();
    let value : Value   = serde_json::from_str(&contents).unwrap();
    let canonical_value: canonical_json::Value = canonical_json::Value::try_from(value).unwrap();
    let canonical_json_str: String = canonical_json::to_string(&canonical_value).unwrap();
    let obj = canonical_value.as_object().unwrap();

    let message: &[u8] = canonical_json_str.as_bytes();
    let sk = secret_key.from_base58check().unwrap();
    let pk = public_key.from_base58check().unwrap();
    let c = &pk.1[3..];
    let d =&sk.1[3..];
    let public_key= ed25519_dalek::PublicKey::from_bytes(c);
    let public_key = match public_key {
        Ok(public_key) => public_key,
        Err(error) => {
            panic!("There was a problem assigning a key: {:?}", error)
        },
    };
    let secret_key:SecretKey = ed25519_dalek::SecretKey::from_bytes(d).unwrap();
    let keypair:Keypair = Keypair {
        secret : secret_key,
        public : public_key,
    };
    let signature: Signature = keypair.sign::<Sha512>(message);
    assert!(public_key.verify::<Sha512>(message, &signature).is_ok());

    let signature_bytes:Vec<u8> = signature.to_bytes().to_vec();
    let encoded = base64::encode(&signature_bytes);

    let dst_address1 = obj.get("dst_address").unwrap();
    let dst_address = str::replace(&dst_address1.to_string(),"\"","");

    let encoded1 = encoded.clone();

    //just to test base64 encoding
    let dec = verify_signature(encoded);
    assert_eq!(dec,signature_bytes);

    let signed_json  = SignedRequestForced {dst_address: dst_address.to_string(),signature: encoded1};
    let j = serde_json::to_string(&signed_json).unwrap();
    println!("{}",j);
    File::create("req.json").unwrap();
    fs::write("./req.json",j).unwrap();
    return ;
}
fn sign_json(secret_key:String,public_key:String,json:String) -> ()  {
    let contents = fs::read_to_string(json).unwrap();
    let value : Value   = serde_json::from_str(&contents).unwrap();
    let canonical_value: canonical_json::Value = canonical_json::Value::try_from(value).unwrap();
    let canonical_json_str: String = canonical_json::to_string(&canonical_value).unwrap();
    let obj = canonical_value.as_object().unwrap();
    let now = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs();

    let timest = format!(",\"timestamp\":{}}}",now.to_string());
    let canonical_json_str = &canonical_json_str[..&canonical_json_str.chars().count()-1];
    let canonical_json_str = format!("{}{}",canonical_json_str,timest);

    let message: &[u8] = canonical_json_str.as_bytes();
    let sk = secret_key.from_base58check().unwrap();
    let pk = public_key.from_base58check().unwrap();
    let c = &pk.1[3..];
    let d =&sk.1[3..];
    let public_key= ed25519_dalek::PublicKey::from_bytes(c);
    let public_key = match public_key {
        Ok(public_key) => public_key,
        Err(error) => {
            panic!("There was a problem assigning a key: {:?}", error)
        },
    };
    let secret_key:SecretKey = ed25519_dalek::SecretKey::from_bytes(d).unwrap();
    let keypair:Keypair = Keypair {
        secret : secret_key,
        public : public_key,
    };
    let signature: Signature = keypair.sign::<Sha512>(message);
    assert!(public_key.verify::<Sha512>(message, &signature).is_ok());

    let signature_bytes:Vec<u8> = signature.to_bytes().to_vec();
    let encoded = base64::encode(&signature_bytes);
    println!("{:#?}",encoded);
    let snd_address1 = obj.get("snd_address").unwrap();
    let snd_address = str::replace(&snd_address1.to_string(),"\"","");
    println!("{:#?}",snd_address);
    let dst_address1 = obj.get("dst_address").unwrap();
    let dst_address = str::replace(&dst_address1.to_string(),"\"","");
    println!("{:#?}",dst_address);
    let tx_value = obj.get("tx_value").unwrap();
    let encoded1 = encoded.clone();

    //just to test base64 encoding
    let dec = verify_signature(encoded);
    assert_eq!(dec,signature_bytes);

    let signed_json  = SignedRequest {snd_address: snd_address.to_string(),dst_address: dst_address.to_string(),tx_value: tx_value.as_u64().unwrap(),timestamp:now,signature: encoded1};
    let j = serde_json::to_string(&signed_json).unwrap();
    println!("{}",j);
    File::create("req.json").unwrap();
    fs::write("./req.json",j).unwrap();
    return ;
}
//TODO - scrap Pub key from tezos RPC API
fn verify_signature(signature_b64:String) -> Vec<u8>{
    let decode = base64::decode(&signature_b64).unwrap();
    return decode;
}
fn main() {
    let matches = App::new("json_signer")
        .version("v1.0")
        .arg(Arg::with_name("INPUT")
            .help("Json file to sign")
            .required(true)
            .index(1))
        .arg(Arg::with_name("PublicKey")
            .help("pass public key to signer")
            .required(true)
            .index(2))
        .arg(Arg::with_name("SecretKey")
            .help("pass secret key to signer")
            .required(true)
            .index(3))
        .arg(Arg::with_name("forced")
            .help("tell signer about type of request (exec,normal)")
            .required(false)
            .index(4))
        .get_matches();
    let mut forced = "normal";
    if matches.is_present("forced") {
        forced = matches.value_of("forced").unwrap();
    }
    let input_json = matches.value_of("INPUT").unwrap();
    let public_key = matches.value_of("PublicKey").unwrap();
    let secret_key = matches.value_of("SecretKey").unwrap();
    if forced == "normal" {
        sign_json(secret_key.to_string(), public_key.to_string(), input_json.to_string());
    }
    else {
        sign_json_forced(secret_key.to_string(), public_key.to_string(), input_json.to_string());
    }
}
